package keshav.image_metadata_reader

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import keshav.image_metadata_reader.controllers.BaseController.Companion.createViewAndController
import keshav.image_metadata_reader.controllers.MainViewController

class MainApplication : Application() {

  override fun start(primaryStage: Stage?) {
    val mainViewFxml = ClassLoader.getSystemResource("views/main.fxml")
    val mainViewContFactory = MainViewController.Factory()
    val rootView = createViewAndController(mainViewContFactory, mainViewFxml)
    primaryStage?.scene = Scene(rootView)
    primaryStage?.show()
  }
}
