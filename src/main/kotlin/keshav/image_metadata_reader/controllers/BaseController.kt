package keshav.image_metadata_reader.controllers

import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import java.net.URL

abstract class BaseController<T : Parent> {
  private var _rootController: T? = null
  protected val rootController: T
    get() = _rootController!!

  abstract class Factory<T : Parent> {
    abstract fun create(): BaseController<T>
  }

  companion object {
    fun <T : Parent> createViewAndController(factory: BaseController.Factory<T>, fxmlFile: URL): T {
      val controller = factory.create()
      val view = FXMLLoader(fxmlFile).let { fxmlLoader ->
        fxmlLoader.setController(controller)
        fxmlLoader.load<T>()
      }
      controller._rootController = view
      return view
    }
  }
}
