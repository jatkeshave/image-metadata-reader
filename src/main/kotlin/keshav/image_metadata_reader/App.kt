/*
 * This Kotlin source file was generated by the Gradle 'init' task.
 */
package keshav.image_metadata_reader

import javafx.application.Application

fun main(args: Array<String>) {
    Application.launch(MainApplication::class.java)
}
