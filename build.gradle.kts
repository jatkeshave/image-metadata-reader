plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.61"
    id("org.openjfx.javafxplugin") version "0.0.8"
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    implementation("com.drewnoakes:metadata-extractor:2.13.0")
}

application {
    mainClassName = "keshav.image_metadata_reader.AppKt"
}

javafx {
    version = "11"
    modules("javafx.controls", "javafx.fxml")
//    configuration = "compileOnly"
}
