@file:JvmName("MainViewController")

package keshav.image_metadata_reader.controllers

import javafx.fxml.FXML
import javafx.scene.control.Alert
import javafx.scene.control.Button
import javafx.scene.layout.AnchorPane
import java.net.URL
import java.util.*

class MainViewController() : BaseController<AnchorPane>(), javafx.fxml.Initializable {
  @FXML
  private lateinit var btnClickMe: Button

  class Factory : BaseController.Factory<AnchorPane>() {
    override fun create(): BaseController<AnchorPane>  = MainViewController()
  }

  override fun initialize(location: URL?, resources: ResourceBundle?) {
    btnClickMe.setOnMouseClicked { event ->
      Alert(Alert.AlertType.INFORMATION, "Button with text ${btnClickMe.text}, opened this alert dialog")
        .show()
    }
  }
}
